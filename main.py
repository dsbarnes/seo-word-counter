import argparse
import requests
import string
from bs4 import BeautifulSoup

def main(args):
    """ Main entry point of the app """
    # Print the total wordcount
    print(word_count(args.url))
    # Get the <main> text of the URL passed as arg
    page = get_body_text_from_page(args.url)
    # Count how many times each word passed occurs in the text
    wordlist = count_keywords(page)
    # Print the formatted output
    format_output(wordlist)


def get_body_text_from_page(url):
    """Returns the text from all elements withing the <main> tag."""
    page = requests.get(url)
    soup = BeautifulSoup(page.text, 'html.parser')
    return soup.body.main.get_text()

def word_count(url):
    page = get_body_text_from_page(url)
    text = page.split(' ')
    return len(text)



def count_keywords(text):
    """Returns a count of the provided keywords"""
    text = text.split(' ')
    remove_chars = string.punctuation + string.digits
    clean_args = [keyword.strip().lower() for keyword in args.wordlist]
    keyword_dict = dict.fromkeys(clean_args, 0)
    for word in text:
        word = word.strip().lower().translate(str.maketrans('', '', remove_chars))
        if word in keyword_dict.keys():
            keyword_dict[word] += 1
    return keyword_dict


def format_output(wordlist):
    for key, val in wordlist.items():
        print(f'{key}: {val}')

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("url", help="The URL to scrape")
    parser.add_argument("wordlist", nargs="+", help="The list of words to return a count for")
    args = parser.parse_args()
    main(args)

